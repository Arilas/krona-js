krona.define("tests/krona/topic", ["krona/topic"], function(topic){
	krona.log("Topic Tests");
	if(topic.publish("test.message") == 0){
		krona.log("Test with empty listener passed");
	} else {
		krona.error(new Exception("Test with empty listener failed"));
	}
	var
		testvalue = 0,
		removeListener = topic.subscribe("test.message", function(){
			testvalue = 1;
		}),
		runCount = topic.publish("test.message")
	;
	if(runCount == 1){
		krona.log("Test with one listener work with publish passed");
	} else {
		krona.error(new Exception("Test with one listener work with publish failed"));
	}
	if(testvalue == 1){
		krona.log("Test affect result of listener passed");
	} else {
		krona.error(new Exception("Test affect result of listener failed"));
	}

	removeListener();

	if(topic.publish("test.message") == 0){
		krona.log("Test publish after removing listener passed");
	} else {
		krona.error(new Exception("Test publish after removing listener failed"));
	}
	return true;
});