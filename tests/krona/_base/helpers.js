define("tests/krona/_base/helpers", ['krona/_base/helpers', 'krona/test/case', 'krona/test/test'], function(helpers, testCase, test){

	var
		helpersTest = new testCase({name: 'Helpers'}),
		testObj = {}
	;
	helpersTest.setTestObject(testObj);
	helpersTest.addTestMethod(function(){
		var numeberTest = new test({name: "helpers isNumber"});
		numeberTest
			.is(helpers.isNumber('5'), true)
			.is(helpers.isNumber(5), true)
			.is(helpers.isNumber('-4.3'), true)
			.is(helpers.isNumber(4.3), true)
			.is(helpers.isNumber('test'), false)
			.is(helpers.isNumber(['arr']), false)
			.is(helpers.isNumber({t: "est"}), false)
		;
		return numeberTest;
	});
	helpersTest.addTestMethod(function(){
		var arrayTest = new test({name: "helpers isArray"});
		arrayTest
			.is(helpers.isArray(['t']), true)
			.is(helpers.isArray('sdf'), false)
			.is(helpers.isArray({t: "est"}), false)
			.is(helpers.isArray(5), false)
		;
		return arrayTest;
	});
	helpersTest.addTestMethod(function(){
		var functionTest = new test({name: "helpers isFunction"});
		functionTest
			.is(helpers.isFunction(function(){}), true)
			.is(helpers.isFunction(['t']), false)
			.is(helpers.isFunction({t: "est"}), false)
		;
		return functionTest;
	});
	helpersTest.addTestMethod(function(){
		var objectTest = new test({name: "helpers isObject"});
		objectTest
			.is(helpers.isObject({t: "ets"}), true)
			.is(helpers.isObject(krona), true)
			.is(helpers.isObject(['test']), false)
			.is(helpers.isObject(5), false)
			.is(helpers.isObject(function(){}), false)
		;
		return objectTest;
	});
	helpersTest.addTestMethod(function(){
		var stringTest = new test({name: "helpers isString"});
		stringTest
			.is(helpers.isString('test'), true)
			.is(helpers.isString(5), false)
			.is(helpers.isString({t: "est"}), false)
			.is(helpers.isString(['t']), false)
		;
		return stringTest;
	});

	return helpersTest.run();
});