define(['krona/aspect', 'krona/test/case', 'krona/test/test'], function(aspect, testCase, test){

	var
		aspectTest = new testCase({name: 'Aspect'}),
		testObj = {
			results: [],
			method: function(){
				this.results.push('orig');
				return 'orig';
			}
		}
	;
	aspectTest.setTestObject(testObj);
	aspectTest.addTestMethod(function(){
		aspect.before('method', this, function(){
			this.results.push('before');
		});
		this.method();
		return new test({name: "aspect Before"}).equal(['before', 'orig'], this.results);
	});

	aspectTest.addTestMethod(function(){
		aspect.before('method', this, function(){
			this.results.push('before');
		});
		aspect.after('method', this, function(result){
			this.results.push('after');
			if(result == 'orig'){
				return 'after';
			}
		});
		var result = this.method();
		return new test({name: 'aspect After'})
			.equal(['before', 'orig', 'after'], this.results)
			.is('after', result);
	});

	aspectTest.addTestMethod(function(){
		aspect.after('method', this, function(result){
			this.results.push('after');
			if(result == 'orig'){
				return 'after';
			}
		});
		var result = this.method();
		return new test({name: 'aspect After with init'})
			.equal(['orig', 'after'], this.results)
			.is('after', result);
	});
	return aspectTest.run();

});