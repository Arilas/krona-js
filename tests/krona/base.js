krona.define("tests/krona/base", function(){
	krona.log("Base Tests");

	if(
		krona.helpers.isNumber('5') &&
		krona.helpers.isNumber(5) &&
		krona.helpers.isNumber('-4.3') &&
		krona.helpers.isNumber(4.3) &&
		!krona.helpers.isNumber('test') &&
		!krona.helpers.isNumber(['arr']) &&
		!krona.helpers.isNumber({t: "est"})
	){
		krona.log('Test Number Helper passed');
	} else {
		krona.error(new Exception('Test Number Helper failed'));
	}

	if(
		krona.helpers.isArray(['t']) &&
		!krona.helpers.isArray('sdf') &&
		!krona.helpers.isArray({t: "est"}) &&
		!krona.helpers.isArray(5)
	){
		krona.log('Test Array Helper passed');
	} else {
		krona.error(new Exception('Test Array Helper failed'));
	}

	if(
		krona.helpers.isFunction(function(){}) &&
		!krona.helpers.isFunction(['t']) &&
		!krona.helpers.isFunction({t: "est"})
	){
		krona.log('Test Function Helper passed');
	} else {
		krona.error(new Exception('Test Function Helper failed'));
	}

	if(
		krona.helpers.isObject({t: "ets"}) &&
		krona.helpers.isObject(krona) &&
		!krona.helpers.isObject(['test']) &&
		!krona.helpers.isObject(5) &&
		!krona.helpers.isObject(function(){})
	){
		krona.log('Test Object Helper passed');
	} else {
		krona.error(new Exception('Test Object Helper failed'));
	}

	if(
		krona.helpers.isString('test') &&
		!krona.helpers.isString(5) &&
		!krona.helpers.isString({t: "est"}) &&
		!krona.helpers.isString(['t'])
	){
		krona.log('Test String Helper passed');
	} else {
		krona.error(new Exception('Test String Helper failed'));
	}

	krona.define('test', function(){
		return 5;
	});

	krona.require(['test'], function(result){
		if(
			krona.helpers.isNumber(result) &&
			result == 5
		){
			krona.log('Require Define Test passed');
		} else {
			krona.error(new Exception('Require Define Test failed'));
		}
	});
});