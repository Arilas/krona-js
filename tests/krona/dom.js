define('tests/krona/dom', ['krona/dom', 'krona/dom/construct'], function(dom, domConstruct){
	var
		testNode = domConstruct.create('div', {
			foo: "bar",
			id: "test"
		})
	;

	dom.get('body').appendChild(testNode);

	if(
		testNode.hasAttribute('foo') &&
		testNode.getAttribute('foo') == 'bar' &&
		dom.get('#test').hasAttribute('foo')
	){
		krona.log('Test Creating Node and Appending to Another Node passed');
	} else {
		krona.error(new Exception('Test Creating Node and Appending to Another Node failed'));
	}

	dom.remove(testNode);
	if(
		dom.get('#test').length == 0
	){
		krona.log('Test Removing Node passed');
	} else {
		krona.error(new Exception('Test Removing Node failed'));
	}
});