krona.define("tests/krona/class", function(){
	krona.log("Class Tests");
	var runProgress = [];
	var News = krona.Class("News",[],{
		title: "Some Test Title",
		authors: {
			'Jim': "Thompson"
		},
		addAuthor: function(name, text){
			this.authors[name] = text;
		},
		testMethod: function(){
			runProgress.push('News');
		}
	});

	var
		News1 = new News(),
		News2 = new News()
	;

	News1.addAuthor('John', 'Thompson');
	if(News1.authors.John && !News2.authors.John){
		krona.log("Test for extending objects passed");
	} else {
		krona.error(new Exception("Test for extending objects failed"));
	}

	var NYNews = krona.Class("NYNews", [News], {
		title: "NY",
		writing: {

		},
		arr: [],
		addAuthor: function(writed, name, text){
			this.inherited([name, text]);
			this.writing[name] = text;
		},
		testMethod: function(){
			this.inherited();
			runProgress.push('NYNews');
		}
	});

	var
		NYNews1 = new NYNews(),
		NYNews2 = new NYNews()
	;

	if(NYNews1.title == "NY"){
		krona.log("Test for replacing string in Extending passed");
	} else {
		krona.error(new Exception("Test for replacing string in Extending failed"));
	}

	NYNews1.arr.push("test");

	if(NYNews1.arr.length == 1 && NYNews2.arr.length == 0){
		krona.log("Test for Array extending passed");
	} else {
		krona.error(new Exception("Test for Array extending failed"));
	}

	NYNews1.addAuthor('Some', 'Another', 'Name');

	if(NYNews1.writing.Another && !NYNews2.writing.Another && ! News1.writing){
		krona.log("Test for replacing methods passed");
	} else {
		krona.error(new Exception("Test for replacing methods failed"));
	}

	if(NYNews1.authors.Another && !NYNews2.authors.Another && ! News1.authors.Another){
		krona.log("Test for inherited call passed");
	} else {
		krona.error(new Exception("Test for inherited failed"));
	}

	var LANews = krona.Class("LANews",[],{
		la: "state",
		testMethod: function(){
			this.inherited();
			runProgress.push('LANews');
		}
	});

	var BBCNews = krona.Class("BBCNews", [NYNews, LANews], {
		testMethod: function(){
			this.inherited();
			runProgress.push('BBCNews');
		}
	});

	var ImportantNews = new BBCNews();

	ImportantNews.testMethod();

	if(runProgress[0] == "News" && runProgress[1] == "NYNews" && runProgress[2] == "LANews" && runProgress[3] == "BBCNews"){
		krona.log("Test for inherited call stack passed");
	} else {
		krona.log(runProgress);
		krona.error(new Exception("Test for inherited call stack failed"));
	}

	return true;
});