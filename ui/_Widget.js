define('ui/_Widget', ['krona/dom', 'krona/dom/style', 'krona/dom/class', 'krona/dom/attr', 'krona/dom/construct'], function(dom, style, domClass, attr, domConstruct){
	/**
	 * @class Widget
	 */
	var Widget = krona.Class('ui._Widget',[],{
		domNode: undefined,
		init: function(){
			if(this.domNode){
				this.domNode = dom.get(this.domNode);
			}
		},
		postCreate: function(){
			if(this.appendTo){
				dom.get(this.appendTo).appendChild(this.domNode);
			}
		},
		setAttr: function(){
			var attrs = arguments[0];
			if(arguments.length == 2){
				attrs = {};
				attrs[arguments[0]] = arguments[1];
			}
			attr.set(this.domNode, attrs);
		},
		removeAttr: function(){
			attr.remove(arguments[0]);
		},
		setStyle: function(){
			var attrs = {};
			if(arguments.length == 2){
				attrs[arguments[0]] = arguments[1];
			} else if(arguments.length == 1 && krona.helpers.isObject(arguments[0])){
				attrs = arguments[0];
			}
			style.set(this.domNode, attrs);
		},
		addClass: function(cl, node){
			if(!node)
				node = this.domNode;
			domClass.add(node, cl);
		},
		removeClass: function(cl, node){
			if(!node)
				node = this.domNode;
			domClass.remove(node, cl);
		},
		hasClass: function(cl, node){
			if(!node)
				node = this.domNode;
			domClass.has(node, cl);
		},
		on: function(event, listener){
			if(!this.events){
				this.events = {};
			}
			if(!this.events[event]){
				this.events[event] = [];
			}
			this.domNode.addEventListener(event, listener);
			this.events[event].push(listener);
		},
		render: function(){
			return this.domNode;
		},
		append: function(child, node){
			if(child.render) child = child.render();
			child = dom.get(child);
			(node || this.domNode).appendChild(child);
		},
		detach: function(child, node){
			child = dom.get(child);
			(node || this.domNode).removeChild(child);
		},
		find: function(selector){
			return dom.get(selector, this.domNode);
		},
		destroy: function(){
			this.destroyEvents();
		},
		destroyEvents: function(){
			krona.helpers.forEach(this.events, krona.helpers.hitch(function(event){
				krona.helpers.forEach(event, krona.helpers.hitch(function(listener){
					this.domNode.removeEventListener(event, listener);
				}, this));
			}, this));
			this.events = {};
		}
	});
	return Widget;
});