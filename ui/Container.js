define([], function(){
	/**
	 * Container Mixin for Manage Childs of some Widget
	 * @mixes Widget
	 * @class Container
	 */
	var Container = krona.Class('ui.Container', [], {
		childs: null,
		containerNode: null,
		init: function(){
			this.childs = [];
		},
		postCreate: function(){
			this.inherited();
			if(!this.containerNode){
				this.containerNode = this.domNode;
			} else if(krona.helpers.isString(this.containerNode)) {
				this.containerNode = this.find(this.containerNode);
			}
		},
		append: function(child){
			this.inherited([child, this.containerNode]);
			return this.childs.push(child);
		},
		getChilds: function(){
			return this.childs;
		},
		detach: function(child){
			if(krona.helpers.isNumber(child)){
				child = this.childs[child];
			}
			this.inherited([child, this.containerNode]);
			this.childs.splice(this.childs.indexOf(child), 1);
		},
		forEach: function(callback){
			this.childs.forEach(callback, this);
		}
	});
	return Container;
});