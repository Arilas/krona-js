Krona JS
==============
Krona JS is a Framework for Front-End development, that have simple and intuitive syntax. Krona JS build as AMD framework, that may helps you to build powerful app with simple-to-change and simple-to-read source code.

Components
--------------
1. Krona - base package;
2. UI - package that contains ready for use Widgets, Containers, Tabs, ets;
3. KronaX - package with experimental modules.

Support
--------------
Krona JS is Open Source framework, so you may contribute to it.