/**
 * Created by gandza on 16.12.13.
 */
(function(global){
	if(!global.krona) global.krona = {};
	if(!global.krona.basePath) global.krona.basePath = '/';
	global.krona.loaded = {};
	global.krona.modules = {};

	var
		requestWaiter = function(/** XMLHttpRequest */request, /** Function */callback){
			request.onreadystatechange = function(){
				if(request.readyState == 4){
					callback();
				}
			};
			request.send();
		},
		load = function(file, /** Function */callback, /** Boolean */async){
			if(arguments.length == 2){
				async = true;
			}
			if(!global.krona.loaded[file]){
				global.krona.loaded[file] = "pending";
				var request = new XMLHttpRequest();
				request.open('GET', location.protocol+'//'+location.host+global.krona.basePath+file+'.js', async);
				requestWaiter(
					request,
					function(){
						global.krona.loaded[file] = request.responseText;
						callback(request.responseText);
					}
				);
			} else if(this.loaded[file] == "pending"){
				var checker = function(){
					if(global.krona.loaded[file] == "pending"){
						setTimeout(checker, 10);
					} else {
						callback(global.krona.loaded[file]);
					}
				};
				checker();
			} else {
				callback(this.loaded[file]);
			}
		},
		helpers = {
			toString: {}.toString,
			isFunction: function(it){
				return helpers.toString.call(it) == "[object Function]";
			},
			isString: function(it){
				return helpers.toString.call(it) == "[object String]";
			},
			isObject: function(it){
				return helpers.toString.call(it) == "[object Object]";
			},
			isNumber: function(it){
				return !isNaN(parseFloat(it)) && isFinite(it);
			},
			isArray: function(it){
				return helpers.toString.call(it) == "[object Array]";
			},
			hitch: function(method, scope){
				return function(){
					return method.apply(scope, arguments);
				}
			}
		},
		fileRegExp = new RegExp('eval \\(https?\\:\\/\\/[\\w\\.]+\\/([\\_\\w\\/]+)\\.js\\:[0-9]+\\:[0-9]+\\)')
		eval_ = new Function('return eval(arguments[0]);'),
		fileEval = function(text, path){
			path = location.protocol+'//'+location.host+global.krona.basePath+path+'.js';
			eval_(text + "\r\n////@ sourceURL=" + path);
		},
		modulesWaiter = function(/** Array */dependencies, /** Function */callback){
			var
				loaded = new Array((dependencies)?dependencies.length||0:0),
				check = false;
			var checker = function(){
				if(check || !dependencies || dependencies.length == 0){
					callback.apply(this, loaded);
				} else {
					check = true;
					dependencies.forEach(function(item, index){
						if(item !== "undefined" && typeof item == 'string'){
							if( global.krona.modules.hasOwnProperty(item)){
								loaded[index] = global.krona.modules[item];
								delete dependencies[index];
							} else {
								check = false;
							}
						}
					});
					setTimeout(checker,10);
				}
			};
			checker();
		},
		require = function(/** Array*/dependencies, /** Function*/callback){
			var loaded = [];
			dependencies.forEach(function(file){
				if(!global.krona.loaded[file] && !global.krona.modules[file]){
					load(
						file,
						function(text){
							fileEval(text, file);
							loaded.push(file);
							if(dependencies.length == loaded.length){
								modulesWaiter(dependencies, function(){
									callback.apply(this, arguments);
								});
							}
						},
						true
					)
				} else {
					loaded.push(file);
				}
			}, this);
			if(loaded.length == dependencies.length && dependencies.length != 0){
				modulesWaiter(dependencies, function(){
					callback.apply(this, arguments);
				});
			} else if(dependencies.length == 0){
				callback.apply(global, []);
			}
		},
		define = function(){
			var
				name,
				dependencies = [],
				callback
				;
			if(arguments.length == 1 && helpers.isFunction(arguments[0])){
				callback = arguments[0];
			} else if(arguments.length == 2 && helpers.isFunction(arguments[1])){
				if(typeof arguments[0] == 'string'){
					name = arguments[0];
				} else if(helpers.isArray(arguments[0])){
					dependencies = arguments[0];
				}
				callback = arguments[1];
			} else if(arguments.length == 3){
				name = arguments[0];
				dependencies = arguments[1];
				callback = arguments[2];
			}
			if(!name){
				var stack = (new Error('js',56)).stack.split('\n');
				stack.forEach(function(line){
					if(!name && fileRegExp.test(line)){
						name = line.match(fileRegExp)[1];
					}
				});
			}
			require(dependencies, function(){
				global.krona.modules[name] = callback.apply(global, arguments);
			});
		}
	;
	global.krona.loaded['krona/require'] = 'loaded';
	global.krona.modules['krona/require'] = require;
	global.require = require;
	global.define = define;
})(window);