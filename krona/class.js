define(['krona/_base/helpers'],function(helpers){
	var global = window;
	var forEach = function(object, callback, scope){
		if(helpers.isArray(object)){
			object.forEach(callback, scope);
		} else {
			for(var property in object){
				if(object.hasOwnProperty(property)){
					callback.apply(scope,[object[property], property]);
				}
			}
		}
	};
	var mixin = function(destination, object){
		helpers.forEach(object,function(item, name){
			if(helpers.isObject(item) && name != "conflicts"){
				destination[name] = helpers.extend({},item);
			} else if(helpers.isArray(item)){
				destination[name] = item.slice();
			} else if(helpers.isString(item)){
				destination[name] = item;
			}
		});
		return destination;
	};
	var
		extendFunction = function(destination, method, name){
			if(!destination.conflicts[name]) destination.conflicts[name] = [];
			method.title = name;
			destination.conflicts[name].push(method);
			destination[name] = method;
		}
	;
	var Class = function(name, extend, object){
		var klass = function(){
			forEach(extend, function(parent){
				mixin(this, parent.prototype);
			}, this);
			mixin(this,object);
			this.construct.apply(this, arguments);
			if(this.init){
				this.init.apply(this, arguments);
			}
			if(this.create){
				this.create.apply(this,arguments);
			}
			if(this.postCreate){
				this.postCreate.apply(this, arguments);
			}
		};
		klass.prototype.constructor = klass;
		klass.prototype.construct = function(){
			helpers.extend(this,arguments[0]);
		};
		klass.prototype.conflicts = {};
		klass.prototype.inherited = function(args){
			var
				caller = arguments.callee.caller,
				found = false,
				i = 0;
			if(this.conflicts[caller.title]){
				while(!found && i<=this.conflicts[caller.title].length){
					if(this.conflicts[caller.title][i] === caller){
						found = true;
						break;
					}
					i++;
				}
				if (found && helpers.isFunction(this.conflicts[caller.title][i-1])){
					return this.conflicts[caller.title][i-1].apply(this, args);
				}
			}
		};
		if(extend != null){
			helpers.forEach(extend, function(parent){
				helpers.forEach(parent.prototype,function(item, name){
					if(helpers.isFunction(item)){
						extendFunction(klass.prototype, item, name);
					} else if(name != "conflicts") {
						klass.prototype[name] = item;
					} else if(name == "conflicts"){
						if(!klass.prototype[name]){
							klass.prototype[name] = {};
						}
						helpers.forEach(item,function(quote,property){
							if(!klass.prototype[name][property]) klass.prototype[name][property] = [];
							klass.prototype[name][property] = klass.prototype[name][property].concat(quote);
						});
					}
				});
			});
		}
		helpers.forEach(object, function(item, name){
			if(helpers.isFunction(item)){
				extendFunction(klass.prototype, item, name);
			} else if(name != "conflicts") {
				klass.prototype[name] = item;
			}
		});
		return klass;
	};

	global.Class = Class;
	return Class;
});