define('krona/dom', function(){
	var dom =  {
		get: function(node, doc){
			if(typeof node == "string"){
				node = (doc || document).querySelectorAll(node);
			}
			if(node.length == 1){
				return node[0];
			} else {
				return node;
			}
		},
		remove: function(node){
			node = dom.get(node);
			if(!node.length){
				node = [node];
			}
			krona.helpers.forEach(node, function(origNode){
				origNode.parentNode.removeChild(origNode);
			});
		},
		replace: function(origNode, newNode){
			origNode = dom.get(origNode);
			newNode = dom.get(newNode);
			if(!origNode.length){
				return origNode.parentNode.replaceChild(newNode,origNode);
			} else {
				krona.error(new Exception('Attempt to replace multiple node'));
			}
		},
		clone: function(origNode){
			origNode = dom.get(origNode);
			return origNode.cloneNode(true);
		}
	};
	return dom;
});