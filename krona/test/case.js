define(['krona/_base/helpers', 'krona/test/test'], function(helpers, test){
	var mixin = function(destination, object){
		helpers.forEach(object,function(item, name){
			if(helpers.isObject(item) && name != "conflicts"){
				destination[name] = helpers.extend({},item);
			} else if(helpers.isArray(item)){
				destination[name] = helpers.extend([], item);
			} else if(krona.helpers.isString(item)){
				destination[name] = item;
			} else if(krona.helpers.isFunction(item)){
				destination[name] = eval('('+item.toString()+')');
			}
		});
		return destination;
	};
	return Class('krona.test.case', [], {
		testObject: null,
		tests: [],
		init: function(){
			if(!this.testObject) this.testObject = {};
			this.tests = [];
		},
		setTestObject: function(object){
			this.testObject = object;
		},
		addTestMethod: function(/** Function */method){
			this.tests.push(method);
		},
		run: function(){
			var result = true;
			this.tests.forEach(function(method){
				var res = method.apply(mixin({}, this.testObject), []);
				if(res.run){
					res = res.run();
				}
				if(result) result = (res || true);
			}, this);
			if(result){
				krona.log('Test Case ' + this.name + ' passed');
			} else {
				krona.error( new Exception('Test Case ' + this.name + ' failed'));
			}
			return result;
		}
	});
});