define(['krona/_base/helpers', 'krona/class'],function(helpers, Class){
	function equal(obj1, obj2){
		var result = true;
		helpers.forEach(obj1, function(item, name){
			if(obj1.hasOwnProperty(name)){
				if(obj2[name] && result){
					if(helpers.isNumber(item) || helpers.isString(item)){
						result = (item == obj2[name]);
					} else if(helpers.isFunction(item)) {
						result = (item.toString() == obj2[name].toString());
						if(result) result = equal(item, obj2[name]);
					} else {
						result = equal(item, obj2[name])
					}
				} else {
					result = false;
				}
			}
		});
		return result;
	}
	var validators = {
		is: function(object){
			if(object.value == object.statement){
				return true;
			} else {
				krona.error(new Exception(object.value + ' != ' + object.statement));
				return false;
			}
		},
		equal: function(object){
			var result = false;
			if(helpers.isNumber(object.obj1) || helpers.isString(object.obj1)){
				result = (object.obj1 == object.obj2);
			} else {
				result = equal(object.obj1, object.obj2);
			}
			if(result){
				return true;
			} else {
				krona.error(new Exception(object.obj1 + ' don\'t equal to: ' + object.obj2));
				return false;
			}
		}
	};
	var test = Class('krona.test.test',[], {
		equals: null,
		init: function(){
			this.equals = [];
		},
		is: function(value, statement){
			this.equals.push({
				type: 'is',
				value: value,
				statement: statement
			});
			return this;
		},
		equal: function(obj1, obj2){
			this.equals.push({
				type: 'equal',
				obj1: obj1,
				obj2: obj2
			});
			return this;
		},
		run: function(){
			var valid = true;
			helpers.forEach(this.equals, helpers.hitch(function(equal){
				var result = validators[equal.type](equal);
				if(valid) valid = result;
			}, this));
			if(valid){
				krona.log('Test ' + this.name + ' passed');
			} else {
				krona.error( new Exception('Test ' + this.name + ' failed'));
			}
			return valid;
		}
	});

	return test;
});