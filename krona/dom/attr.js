krona.define('krona/dom/attr', ['krona/dom'], function(dom){
	return {
		set: function(node, attrs){
			node = dom.get(node);
			if(!node.length){
				node = [node];
			}
			krona.helpers.forEach(node, function(origNode){
				krona.helpers.forEach(attrs, function(value, property){
					origNode.setAttribute(property, value);
				});
			});
		},
		get: function(node, attr){
			node = dom.get(node);
			if(!node.length){
				node = [node];
			}
			var
				name = attr.toLowerCase(),
				values = []
			;
			krona.helpers.forEach(node, function(origNode){
				values.push(origNode.getAttribute(name));
			});
			if(values.length == 1){
				return values[0];
			} else {
				return values;
			}
		},
		remove: function(node, attr){
			node = dom.get(node);
			if(!node.length){
				node = [node];
			}
			krona.helpers.forEach(node, function(origNode){
				origNode.removeAttribute(attr);
			});
		}
	}
});