krona.define('krona/dom/construct', ['krona/dom/attr'], function(attr){
	return {
		toDom: function(frag, doc){
			doc = doc || document;
			frag+="";
			var master = doc.createElement("div");
			master.innerHTML = frag;
			return master.firstChild;
		},
		create: function(tag, attrs){
			attrs = attrs || [];
			if(krona.helpers.isString(tag)){
				tag = document.createElement(tag);
			}
			attr.set(tag, attrs);
			return tag;
		}
	}
});