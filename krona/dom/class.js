define(['krona/dom'], function(dom){
	var
		setClass = (function(){
			var testNode = document.createElement('div');
			if(testNode.classList && testNode.classList.add){
				return function(node, classes){
					krona.helpers.forEach(classes, function(cl){
						node.classList.add(cl);
					});
				}
			} else {
				return function(node, classes){
					krona.helpers.forEach(classes, function(cl){
						node.className = node.className.concat(" "+cl);
					});
				}
			}
		})(),
		removeClass = (function(){
			var testNode = document.createElement('div');
			if(testNode.classList && testNode.classList.add){
				return function(node, classes){
					krona.helpers.forEach(classes, function(cl){
						node.classList.remove(cl);
					})
				}
			} else {
				return function(node, classes){
					krona.helpers.forEach(classes, function(cl){
						if(node.className.indexOf(cl) != -1){
							node.className.slice(node.className.indexOf(cl), cl.length);
						}
					});
				}
			}
		})()
	;

	return {
		add: function(node, classes){
			if(!krona.helpers.isArray(classes)){
				classes = [classes];
			}
			setClass(dom.get(node), classes);
		},
		remove: function(node, classes){
			if(!krona.helpers.isArray(classes)){
				classes = [classes];
			}
			removeClass(dom.get(node), classes);
		},
		has: function(node, cl){
			return (dom.get(node).className.indexOf(cl) != -1);
		}
	}
});