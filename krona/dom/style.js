krona.define(['krona/dom'], function(dom){
	var style = {
		set: function(node, attrs){
			node = dom.get(node);
			krona.helpers.forEach(attrs, function(value, attr){
				node.style[attr] = value;
			});
		}
	};
	return style;
});