krona.define("krona/topic",function(){
	var
		topic = {
			feed: {},
			subscribe: function(message, callback){
				if(!topic.feed[message]){
					topic.feed[message] = [];
				}
				var id = topic.feed[message].push(callback);
				return function(){
					topic.unsubscribe(message,id-1);
				};
			},
			publish: function(message, data){
				var count = 0;
				if(topic.feed[message]){
					topic.feed[message].forEach(function(callback){
						count++;
						callback(data || []);
					})
				}
				return count;
			},
			unsubscribe: function(message, id){
				topic.feed[message].splice(id,1);
			}
		}
	;
	return topic;
});