(function(global, configs){
	var

		Exception = function(message, code){
			this.code = code || 0;
			this.message = message;
		},
		simpleLoader = function(path, async){
			var req = new XMLHttpRequest();
			path = location.protocol+'//'+location.host+configs.basePath+path+'.js';
			req.open('GET', path, async);
			req.onreadystatechange = function(){
				if(req.readyState == 4){
					eval(req.responseText + "\r\n////@ sourceURL=" + path)
				}
			};
			req.send();
		}
	;
	Exception.prototype.getMessage = function(){
		return this.message;
	};
	Exception.prototype.getCode = function(){
		return this.code;
	};
	var krona = function(){
		this.version = "0.1";
		this.modules = {};
		this.loaded = {};
		this.basePath = configs.basePath || "/";
	};
	krona.prototype.constructor = krona;
	krona.prototype.log = function(){
		if(configs.debug && global.console && global.console.log){
			Array.prototype.unshift.call(arguments,"[Info]: ");
			global.console.log.apply(global.console, arguments);
		}
	};
	krona.prototype.error = function(/** Exception */e){
		if(configs.debug && global.console && global.console.error){
			global.console.error(e.getCode() + ": " + e.getMessage());
		}
	};
	global.krona = new krona();
	global.Exception = Exception;

	if(!global.require){
		//Init Krona Require:
		simpleLoader('krona/require', false);
	}
})(
	window,
	{
		basePath: "/",
		debug: true
	}
);