krona.define(function(){
	var array = {
		forEach: function(arr, callback, scope){
			//summary:
			//		Fox every item in arr, callback is invoked.
			if(scope){
				for(var idx in arr){
					callback.apply(scope,[arr[idx], idx, arr]);
				}
			} else {
				for(var idx in arr){
					callback(arr[idx], idx, arr);
				}
			}
		},
		indexOf: function(value, arr){
			var key = -1;
			this.forEach(arr, function(content, property){
				if(content === value){
					key = property;
				}
			});
			return key;
		}
	};
	return array;
});