/**
 * Created by gandza on 16.12.13.
 */
define(function(){
	var helpers = {
		toString: {}.toString,
		isFunction: function(it){
			return helpers.toString.call(it) == "[object Function]";
		},
		isString: function(it){
			return helpers.toString.call(it) == "[object String]";
		},
		isObject: function(it){
			return helpers.toString.call(it) == "[object Object]";
		},
		isNumber: function(it){
			return !isNaN(parseFloat(it)) && isFinite(it);
		},
		isArray: function(it){
			return helpers.toString.call(it) == "[object Array]";
		},
		hitch: function(method, scope){
			return function(){
				return method.apply(scope, arguments);
			}
		},
		forEach: function(vector, callback){
			for(var p in vector){
				callback(vector[p],p);
			}
		},
		extend: function(/** Object */destination, /** Object */object){
			helpers.forEach(object,function(item, name){
				if(helpers.isObject(item) && name != "conflicts"){
					destination[name] = helpers.extend({},item);
				} else if(helpers.isArray(item)){
					destination[name] = item.slice();
				} else {
					destination[name] = item;
				}
			});
			return destination;
		}
	};
	return helpers;
});