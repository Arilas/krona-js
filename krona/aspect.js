define(["krona/_base/array"],function(array){
	return {
		before: function(method, scope, callback){
			var
				name;
			if(krona.helpers.isFunction(method)){
				name = array.indexOf(method,scope);
			} else {
				name = method;
				method = scope[method];
			}
			if(!method.origin){
				var replacement = function(){
					var result;
					scope[name].before.apply(scope,arguments);
					result = scope[name].origin.apply(scope,arguments);
					result = scope[name].after.apply(scope,[result]);
					return result;
				};
				replacement.origin = method;
				replacement.after = function(result){return result;};
				replacement.before = callback;
				scope[name] = replacement;
			} else {
				scope[name].before = callback;
			}
		},
		after: function(method, scope, callback){
			var
				name;
			if(krona.helpers.isFunction(method)){
				name = array.indexOf(method,scope);
			} else {
				name = method;
				method = scope[method];
			}
			if(!method.origin){
				var replacement = function(){
					var result;
					scope[name].before.apply(scope,arguments);
					result = scope[name].origin.apply(scope,arguments);
					result = scope[name].after.apply(scope,[result]);
					return result;
				};
				replacement.origin = method;
				replacement.after = callback;
				replacement.before = function(){};
				scope[name] = replacement;
			} else {
				scope[name].after = callback;
			}
		}
	}
});